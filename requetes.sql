
-- Ce fichier qui contient l'ensemble des requêtes que nous avons utilisées sur superset pour rédiger le rapport.

--  2.1.1. Les 10 jeux les plus recommandés

SELECT "app_name" AS "Nom du jeu",
       "nb_recommended" AS "Nombre de recommandations",
       "ratio_reco" AS "Pourcentage de review positive"
      
       
FROM
  (SELECT app_name,
          COUNT(recommended) AS nb_recommended,
          SUM(CASE
                  WHEN recommended = 'true' THEN 1.0
                  ELSE 0.0
              END) / COUNT(recommended) * 100 AS ratio_reco
   FROM shared
   GROUP BY app_name
   HAVING COUNT(recommended) > 10000
   ORDER BY ratio_reco DESC
   LIMIT 10) 

--  2.1.2. Les 10 jeux les moins recommandés

SELECT "app_name" AS "Nom du jeu",
       "nb_recommended" AS "Nombre de recommandations",
       "ratio_reco" AS "Pourcentage de review positive"
      
       
FROM
  (SELECT app_name,
          COUNT(recommended) AS nb_recommended,
          SUM(CASE
                  WHEN recommended = 'true' THEN 1.0
                  ELSE 0.0
              END) / COUNT(recommended) * 100 AS ratio_reco
   FROM shared
   GROUP BY app_name
   HAVING COUNT(recommended) > 10000
   ORDER BY ratio_reco ASC
   LIMIT 10) 

-- 2.2.1 impact early access

SELECT "recommended" AS "recommended",
       count("recommended") AS "COUNT(recommended)"
FROM "druid"."shared"
WHERE "written_during_early_access" = 'true'
GROUP BY "recommended"
ORDER BY "COUNT(recommended)" DESC
LIMIT 10000;



SELECT "recommended" AS "recommended",
       count("recommended") AS "COUNT(recommended)"
FROM "druid"."shared"
WHERE "written_during_early_access" = 'false'
GROUP BY "recommended"
ORDER BY "COUNT(recommended)" DESC
LIMIT 10000;

-- 2.2.2 impact reçu gratuitement

SELECT "recommended" AS "recommended",
       sum("nombre de recommandation") AS "SUM(nombre de recommandation)"
FROM
  (SELECT received_for_free,
          recommended,
          COUNT(recommended) AS "nombre de recommandation"
   FROM "shared"
   GROUP BY recommended,
            received_for_free) AS "virtual_table"
WHERE "received_for_free" = 'true'
GROUP BY "recommended"
ORDER BY "SUM(nombre de recommandation)" DESC



SELECT "recommended" AS "recommended",
       sum("nombre de recommandation") AS "SUM(nombre de recommandation)"
FROM
  (SELECT received_for_free,
          recommended,
          COUNT(recommended) AS "nombre de recommandation"
   FROM "shared"
   GROUP BY recommended,
            received_for_free) AS "virtual_table"
WHERE "received_for_free" = 'false'
GROUP BY "recommended"
ORDER BY "SUM(nombre de recommandation)" DESC

-- 2.2.3 impact de la langue

SELECT "language" AS "language",
       sum("Pourcentage de recommandation") AS "SUM(Pourcentage de recommandation)"
FROM
  (SELECT "language",
          SUM(CASE
                  WHEN recommended='true' THEN 1.0
                  ELSE 0.0
              END) / COUNT(recommended) * 100 AS "Pourcentage de recommandation",
          COUNT(recommended) AS "nombre de recommandation"
   FROM "shared"
   GROUP BY 1
   ORDER BY "nombre de recommandation" DESC
   LIMIT 10) AS "virtual_table"
GROUP BY "language"
ORDER BY "SUM(Pourcentage de recommandation)" DESC
LIMIT 100;

-- 2.2.4 temps de jeu

SELECT "fourchette_heure_de_jeu" AS "fourchette_heure_de_jeu",
       AVG("Pourcentage recommandation") AS "AVG(Pourcentage recommandation)"
FROM
  (SELECT CASE
              WHEN "author.playtime_at_review" <= 100 THEN '0-100'
              WHEN "author.playtime_at_review" <= 200
                   AND "author.playtime_at_review" > 100 THEN '101-200'
              WHEN "author.playtime_at_review" <= 300
                   AND "author.playtime_at_review" > 200 THEN '201-300'
              WHEN "author.playtime_at_review" <= 500
                   AND "author.playtime_at_review" > 300 THEN '301-500'
              WHEN "author.playtime_at_review" > 500 THEN '501+'
          END AS "fourchette_heure_de_jeu",
          SUM(CASE
                  WHEN recommended='true' THEN 1.0
                  ELSE 0.0
              END) / COUNT(recommended) * 100 AS "Pourcentage recommandation"
   FROM "shared"
   GROUP BY "author.playtime_at_review") AS "virtual_table"
GROUP BY "fourchette_heure_de_jeu"
ORDER BY "AVG(Pourcentage recommandation)" DESC
LIMIT 10000;

-- 2.3.1 vote utile

SELECT 
  "app_name",
  "votes_helpful",
  "review"
FROM 
  "shared"
WHERE "votes_helpful" > 10000 AND "votes_helpful" < 4000000000
GROUP BY "votes_helpful",
  "app_name",
  "review"
ORDER BY max("votes_helpful") DESC
LIMIT 3

-- 2.3.2 vote funny

SELECT 
  "app_name",
  "votes_funny",
  "review"
FROM 
  "shared"
WHERE "votes_funny" > 10000 AND "votes_funny" < 4000000000
GROUP BY "votes_funny",
  "app_name",
  "review"
ORDER BY max("votes_funny") DESC
LIMIT 3

-- 2.3.3 nombre de commentaires

SELECT
  "comment_count",
  "review",
  "app_name"
FROM "shared"
WHERE "comment_count" > 1000
GROUP BY "review", "comment_count", "app_name"
ORDER BY "comment_count" DESC
LIMIT 3

-- 2.3.4 reviews négatives avec le plus d'heures de jeu

SELECT "app_name" AS "app_name",
       "review" AS "review",
       "author.playtime_at_review" AS "author.playtime_at_review"
FROM "druid"."shared"
WHERE "author.playtime_at_review" > 500000
  AND "recommended" = 'false'
GROUP BY "app_name",
         "review",
         "author.playtime_at_review"
ORDER BY max("author.playtime_at_review") DESC
LIMIT 3;



