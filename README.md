# Projet Big Data

Dataset utilisé : https://www.kaggle.com/datasets/najzeko/steam-reviews-2021

Membres du projet : 
- ROBAEY Dennis
- VANDEWALLE Charles


Ce projet contient l'ensemble du code source utilisé pour le projet Big Data (module en deuxième année de master).

Comme nous n'avons utilisé que Superset / Druid, le fichier requetes.sql qui contient l'ensemble des requêtes que nous avons utilisées sur Superset pour rédiger le rapport. Nous avons également ajouté les graphiques / tables générés par Superset dans le dossier big_data_graph.

